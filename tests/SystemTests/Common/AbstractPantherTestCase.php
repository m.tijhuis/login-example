<?php

namespace App\Tests\SystemTests\Common;

use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeOutException;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Symfony\Component\Panther\Client;
use Symfony\Component\Panther\DomCrawler\Crawler;
use Symfony\Component\Panther\PantherTestCase;

/**
 * @SuppressWarnings(PHPMD.MemberPrimaryPrefix)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class AbstractPantherTestCase extends PantherTestCase
{

    /** @var Client */
    protected $clientOne;

    /** @var Client */
    protected $clientTwo;

    public function setUp()
    {
        if (empty($this->clientOne)) {
            $this->clientOne = Client::createChromeClient(null, null, [], 'http://localhost:8000');
        }

        if (empty($this->clientTwo)) {
            $this->clientTwo = Client::createChromeClient(null, null, ['port'=> 9516], 'http://localhost:8000');
        }
    }

    public function requestPage(Client $client, $path): Crawler
    {
        return $client->request('GET', $path);
    }

    public function submitFormWithData(Crawler $crawler, string $submitButtonText, array $values): void
    {
        $submitButton = $crawler->selectButton($submitButtonText);
        $form         = $submitButton->form();

        $form->setValues($values);

        $submitButton->click();
    }

    public function login(Client $client, string $username, string $password): void
    {
        $crawler = $this->requestPage($client, '/');

        $this->loginWithEmailPassword($crawler, $username, $password);

        $this->waitForTitle($client, 'Dashboard');
    }

    public function loginWithEmailPassword(Crawler $crawler, string $email, string $password): void
    {
        $this->submitFormWithData($crawler, 'btn-login', ['email' => $email, 'password' => $password]);
    }

    public function logout(Client $client): void
    {
        $client->findElement(WebDriverBy::cssSelector('header.navbar img.img-avatar'))->click();
        $client->findElement(WebDriverBy::linkText('Uitloggen'))->click();
    }

    public function waitForTitle(Client $client, string $title, int $timeoutInSecond = 5, int $intervalInMillisecond = 250): void
    {
        $client->wait($timeoutInSecond, $intervalInMillisecond)->until(WebDriverExpectedCondition::titleIs($title));
    }

    public function waitForAlert(Client $client, string $text, int $timeoutInSecond = 5, int $intervalInMillisecond = 250): void
    {
        $client->wait($timeoutInSecond, $intervalInMillisecond)->until(
            WebDriverExpectedCondition::elementTextContains(WebDriverBy::cssSelector('.alert'), $text)
        );
    }

    public function clickOnLinkText(Client $client, $linkText): void
    {
        $client->findElement(WebDriverBy::linkText($linkText))->click();
    }

    public function getTitle(Client $client): string
    {
        return $client->getTitle();
    }

    public function assertMenuItemExists(Client $client, string $name): void
    {
        $this->assertTrue($client->findElement(WebDriverBy::linkText($name))->isDisplayed());
    }

    public function assertElementNotExists(Client $client, WebDriverBy $locator): void
    {
        $elements = $client->findElements($locator);
        $this->assertCount(0, $elements);
    }

    public function assertElementExists(Client $client, WebDriverBy $locator): void
    {
        $elements = $client->findElements($locator);
        $this->assertNotCount(0, $elements);
    }

    /**
     * @param Client $client
     *
     * @throws NoSuchElementException
     * @throws TimeOutException
     */
    public function waitForDataTableLoaded(Client $client): void
    {
        $client->waitFor('table.dataTable tbody tr', 5);
    }

    public function assertDataTableHasItems(Client $client, int $items): void
    {
        $this->assertCount($items, $client->findElements(WebDriverBy::cssSelector('table.dataTable tbody tr')));
    }

    public function assertDataTableContainsRowWith(Client $client, array $values): void
    {
        $rows      = $client->findElements(WebDriverBy::cssSelector('table.dataTable tbody tr'));
        $rowExists = false;

        foreach ($rows as $row) {
            if ($this->rowContainsValues($row, $values)) {
                $rowExists = true;
            }
        }

        $this->assertTrue($rowExists);
    }

    public function tearDown(): void
    {
        $this->clientOne->quit();
        $this->clientTwo->quit();
    }

    private function rowContainsValues(WebDriverElement $row, array $values): bool
    {
        $cells = $row->findElements(WebDriverBy::cssSelector('td'));
        foreach ($cells as $cell) {
            foreach ($values as $field => $value) {
                if ($cell->getText() === $value) {
                    unset($values[$field]);
                }
            }
        }

        return 0 === count($values);
    }
}
