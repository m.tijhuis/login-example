<?php

namespace App\Tests\SystemTests;

use App\Tests\SystemTests\Common\AbstractPantherTestCase;

class LoginTest extends AbstractPantherTestCase
{
    public function testUserCanLoginInOnlyOneScreen(): void
    {
        $crawler = $this->requestPage($this->clientOne, '/');

        $this->loginWithEmailPassword($crawler, 'info@2solar.nl', 'test1234');

        $this->waitForTitle($this->clientOne,'Dashboard');

        $crawler2 = $this->requestPage($this->clientTwo,'/');

        $this->loginWithEmailPassword($crawler2, 'info@2solar.nl', 'test1234');
        $this->waitForTitle($this->clientOne, 'Dashboard');

        $crawler = $this->requestPage($this->clientOne, '/');
        $this->waitForTitle($this->clientOne, 'Log in!');
    }
}
