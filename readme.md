# Login example project
This project is created to demonstrate the possibility to have single session policy. After an second login, the first login will be logout.
## Installation

### Clone the repository
> git clone git@gitlab.com:m.tijhuis/login-example.git
> cd login-example

### Install dependencies
> composer install

### Install database 
Place the `data.db` file into the folder `/var`

## Run the server
To run the server, execute from the root folder `bin/console server:run`. The server will be available at http://127.0.0.1:8000

## Run tests
To execute the tests, run `bin/phpunit`. The server should be run local