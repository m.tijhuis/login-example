<?php

namespace App\EventListener;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SessionListener implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => ['sessionMatch'],
        ];
    }

    public function sessionMatch(ResponseEvent $event): void
    {
        if ($this->tokenStorage->getToken() && ($user = $this->tokenStorage->getToken()->getUser()) instanceof User
            && $event->getRequest()->getRequestUri() !== '/login') {

            if ($event->getRequest()->getSession()->get('uniqueId') !== $user->getUniqueLoginId()) {
                $response = new RedirectResponse('/logout');
                $event->setResponse($response);
            }
        }
    }
}